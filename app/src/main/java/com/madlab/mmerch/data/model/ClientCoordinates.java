package com.madlab.mmerch.data.model;

import android.location.Location;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Random;

@Entity
public class ClientCoordinates {
    @PrimaryKey(autoGenerate = true)
    private final int id;
    @SerializedName("longitude")
    private final String longitude;
    @SerializedName("latitude")
    private final String latitude;

    public ClientCoordinates(int id, String longitude, String latitude){
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Ignore
    public ClientCoordinates(String longitude, String latitude){
        id = new Random().nextInt();
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Location getLocation(){
        Location location = new Location("");
        location.setLongitude(Double.valueOf(longitude));
        location.setLatitude(Double.valueOf(latitude));
        return location;
    }

    public int getId() {
        return id;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }



}
