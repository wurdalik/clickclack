package com.madlab.mmerch.data.net;

import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.madlab.mmerch.data.model.Visit;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VisitSerializer implements JsonSerializer<Visit> {
    private final static String TAG = "VisitSerializer";

    @Override
    public JsonElement serialize(Visit src, Type typeOfSrc, JsonSerializationContext context) {
        if(src!=null && context!=null){
           SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());

           JsonObject jsonObject = new JsonObject();
           jsonObject.addProperty("clientId", src.getClient().getId());
           jsonObject.addProperty("managerCode", src.getManagerID());
           JsonArray projects = new JsonArray();
           for(String s:src.getProjects()){
               projects.add(s);
           }
           jsonObject.add("projects", projects);
           jsonObject.addProperty("visitId", src.getId());
           JsonObject coordinates = new JsonObject();
           coordinates.addProperty("latitude", src.getCoordinates_latitude());
           coordinates.addProperty("longitude", src.getCoordinates_longitude());
           jsonObject.add("coordinatesManager", coordinates);
           jsonObject.addProperty("coincidence", src.isCoincidence());
           jsonObject.addProperty("error", src.getError());
           jsonObject.addProperty("difference", src.getDifference());
           jsonObject.addProperty("startTime", dateFormat.format(new Date(src.getStartTime())));
           jsonObject.addProperty("stopTime", dateFormat.format(new Date(src.getEndTime())));

            Log.d(TAG, "serialize: "+ jsonObject.toString());

           return jsonObject;
        }
        return null;
    }
}
