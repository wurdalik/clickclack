package com.madlab.mmerch.data;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

public class LoginRequest{

    public static class ServerLoginRequest {
        @SerializedName("userCode")
        private String userCode;
        @SerializedName("password")
        private String password;

        //fixme
        public ServerLoginRequest(String email){
            this.userCode = email;
        }

        public String getUserCode() {
            return userCode;
        }

        public void setUserCode(String userCode) {
            this.userCode = userCode;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }


        public JsonObject getJson(){
            JsonObject login = new JsonObject();
            login.addProperty("code", userCode);

            return login;
        }
    }
}
