package com.madlab.mmerch.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Clients {
    @SerializedName("clients")
    private List<Client>clients;

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }

}
