package com.madlab.mmerch.data.model;

public class Task {
    private String name;
    private Type type;

    enum Type{
        PHOTO, GOODS, QUESTIONS
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
