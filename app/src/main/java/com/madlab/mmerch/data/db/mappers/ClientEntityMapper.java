package com.madlab.mmerch.data.db.mappers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mmerch.data.db.DataBase;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ClientEntityMapper {
    private static final String TAG = "ClientEntityMapper";

    public static ArrayList<Client> getAllClients(SQLiteDatabase database) {
        ArrayList<Client> clients = new ArrayList<>();
        try (Cursor cursor = database.query(true, DataBase.ClientEntity.TABLE_NAME, null, null, null, null, null, DataBase.ClientEntity.TITLE, null)) {
            if (cursor.getCount() > 0) {
                int id = cursor.getColumnIndex(DataBase.ClientEntity.ID);
                int address = cursor.getColumnIndex(DataBase.ClientEntity.ADDRESS);
                int title = cursor.getColumnIndex(DataBase.ClientEntity.TITLE);
                int projects = cursor.getColumnIndex(DataBase.ClientEntity.PROJECTS);
                int isActive = cursor.getColumnIndex(DataBase.ClientEntity.IS_ACTIVE);
                int coordinates_latitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LATITUDE);
                int coordinates_longitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LONGITUDE);

                while (cursor.moveToNext()) {
                    ArrayList<Project> projectList = new ArrayList<>();
                    if(cursor.getString(projects)!=null){
                        for(String s:cursor.getString(projects).split(";")){
                            Project project = ProjectsEntityMapper.getProjectByID(database, s);
                            projectList.add(project);
                        }
                    }

                    Client client = new Client(
                            cursor.getString(id),
                            cursor.getString(address),
                            cursor.getString(title),
                            projectList,
                            cursor.getInt(isActive)==1,
                            cursor.getDouble(coordinates_latitude),
                            cursor.getDouble(coordinates_longitude)
                    );
                    clients.add(client);
                }
            }
        } catch (SQLException e) {
            Log.d(TAG, "@see #getAllClients()" + e.getLocalizedMessage());
        }
        return clients;
    }

    public static ArrayList<Client> getActiveClients(SQLiteDatabase database){
        ArrayList<Client> clients = new ArrayList<>();
        try (Cursor cursor = database.query(true, DataBase.ClientEntity.TABLE_NAME, null, "is_active = 1", null, null, null, DataBase.ClientEntity.TITLE, null)) {
            if (cursor.getCount() > 0) {
                int id = cursor.getColumnIndex(DataBase.ClientEntity.ID);
                int address = cursor.getColumnIndex(DataBase.ClientEntity.ADDRESS);
                int title = cursor.getColumnIndex(DataBase.ClientEntity.TITLE);
                int projects = cursor.getColumnIndex(DataBase.ClientEntity.PROJECTS);
                int isActive = cursor.getColumnIndex(DataBase.ClientEntity.IS_ACTIVE);
                int coordinates_latitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LATITUDE);
                int coordinates_longitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LONGITUDE);

                while (cursor.moveToNext()) {
                    ArrayList<Project> projectList = new ArrayList<>();
                    if(cursor.getString(projects)!=null){
                        for(String s:cursor.getString(projects).split(";")){
                            Project project = ProjectsEntityMapper.getProjectByID(database, s);
                            projectList.add(project);
                        }
                    }

                    Client client = new Client(
                            cursor.getString(id),
                            cursor.getString(address),
                            cursor.getString(title),
                            projectList,
                            cursor.getInt(isActive)==1,
                            cursor.getDouble(coordinates_latitude),
                            cursor.getDouble(coordinates_longitude)
                    );
                    clients.add(client);
                }
            }
        } catch (SQLException e) {
            Log.d(TAG, "@see #getAllClients()" + e.getLocalizedMessage());
        }
        return clients;
    }

    public static Client getClientByID(SQLiteDatabase database, String clientID) {
        Client client = null;
        try (Cursor cursor = database.query(DataBase.ClientEntity.TABLE_NAME, null, "id = ?", new String[]{clientID}, null, null, null, null)) {
            if (cursor.getCount() > 0) {
                int id = cursor.getColumnIndex(DataBase.ClientEntity.ID);
                int address = cursor.getColumnIndex(DataBase.ClientEntity.ADDRESS);
                int title = cursor.getColumnIndex(DataBase.ClientEntity.TITLE);
                int projects = cursor.getColumnIndex(DataBase.ClientEntity.PROJECTS);
                int isActive = cursor.getColumnIndex(DataBase.ClientEntity.IS_ACTIVE);
                int coordinates_latitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LATITUDE);
                int coordinates_longitude = cursor.getColumnIndex(DataBase.ClientEntity.COORDINATES_LONGITUDE);

                while (cursor.moveToNext()) {
                    ArrayList<Project> projectList = new ArrayList<>();
                    if(cursor.getString(projects)!=null){
                        for(String s:cursor.getString(projects).split(";")){
                            Project project = ProjectsEntityMapper.getProjectByID(database, s);
                            projectList.add(project);
                        }
                    }

                    client = new Client(
                            cursor.getString(id),
                            cursor.getString(address),
                            cursor.getString(title),
                            projectList,
                            cursor.getInt(isActive)==1,
                            cursor.getDouble(coordinates_latitude),
                            cursor.getDouble(coordinates_longitude)
                    );
                }
            }
        } catch (SQLException e) {
            Log.d(TAG, "getClientByID: " + e.getLocalizedMessage());
        }
        return client;
    }

    public static void saveClients(SQLiteDatabase database, List<Client> clients) {
        deleteAllClients(database);
        for (Client client : clients) {
            insert(database, client);
        }
    }

    public static void insert(SQLiteDatabase database, Client client) {
        try {
            if(client.getProjects().size()>0){
                ProjectsEntityMapper.insertProject(database, client.getProjects());
            }
            ContentValues contentValues = new ContentValues(7);
            contentValues.put(DataBase.ClientEntity.ID, client.getId());
            contentValues.put(DataBase.ClientEntity.ADDRESS, client.getAddress());
            contentValues.put(DataBase.ClientEntity.TITLE, client.getTitle());
            contentValues.put(DataBase.ClientEntity.PROJECTS, client.getProjectsString());
            contentValues.put(DataBase.ClientEntity.IS_ACTIVE, client.isActive()?1:0);
            contentValues.put(DataBase.ClientEntity.COORDINATES_LATITUDE, client.getCoordinates_latitude());
            contentValues.put(DataBase.ClientEntity.COORDINATES_LONGITUDE, client.getCoordinates_longitude());

            database.insert(DataBase.ClientEntity.TABLE_NAME, null, contentValues);
        }
        catch (RuntimeException e){
            Log.d(TAG, "insert: "+ e.getLocalizedMessage());
        }
    }

    public static boolean haveProjects(SQLiteDatabase database, String clientID){
        return getClientByID(database, clientID).getProjects().size()>0;
    }

    public static void deleteAllClients(SQLiteDatabase database){
        database.delete(DataBase.ClientEntity.TABLE_NAME, null, null);
    }

}
