package com.madlab.mmerch.data.model;

import java.util.UUID;

public class NewVisit {
    private String visitId;
    private String clientCode;
    private String managerId;
    private String latitude;
    private String longitude;
    private String project;

    private boolean coinciedence;
    private String error;
    private String difference;
    private String startTime;
    private String endTime;

    public NewVisit(String clientCode, String managerId, String longitude, String latitude, String project, boolean coinciedence, String error, String difference, String startTime, String endTime ){
        visitId = UUID.randomUUID().toString();
        this.clientCode = clientCode;
        this.managerId = managerId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.project = project;
        this.coinciedence = coinciedence;
        this.error = error;
        this.difference = difference;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public boolean isCoinciedence() {
        return coinciedence;
    }

    public void setCoinciedence(boolean coinciedence) {
        this.coinciedence = coinciedence;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
