package com.madlab.mmerch.data.net;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Project;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ClientDeserializer implements JsonDeserializer<Client> {
    @Override
    public Client deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if(json!=null && context!=null){
            ArrayList<Project> projects = new ArrayList<>();
            //Client
            JsonObject sourceJson = json.getAsJsonObject();
            String id = sourceJson.get("_id").getAsString();
            String address = sourceJson.get("mailingAddress").getAsString();
            String title = sourceJson.get("title").getAsString();
            boolean isActive = sourceJson.get("active").getAsBoolean();

            //Projects
            JsonArray jsonProjects = sourceJson.get("projects").getAsJsonArray();
            for(JsonElement element:jsonProjects){
                String projectId = element.getAsJsonObject().get("id").getAsString();
                String projectTitle = element.getAsJsonObject().get("title").getAsString();
                Project project = new Project(projectId, projectTitle);
                projects.add(project);
            }

            JsonObject coordinates = sourceJson.get("coordinates").getAsJsonObject();
            double coordinates_latitude;
            double coordinates_longitude;
            if(!coordinates.get("latitude").getAsString().equalsIgnoreCase("N")&&!coordinates.get("longitude").getAsString().equalsIgnoreCase("N")){
                coordinates_latitude = Double.parseDouble(coordinates.get("latitude").getAsString().replace(",", "."));
                coordinates_longitude = Double.parseDouble(coordinates.get("longitude").getAsString().replace(",", "."));
            }
            else {
                coordinates_latitude = 0;
                coordinates_longitude = 0;
            }

            return new Client(id, address, title, projects, isActive, coordinates_latitude, coordinates_longitude);
        }
        else {
            throw new JsonParseException("Json or context equals null!");
        }
    }
}
