package com.madlab.mmerch.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "mmerch.db";
    private static int DB_VERSION = 1;

    public DataBaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBase.ClientEntity.CREATE);
        db.execSQL(DataBase.VisitEntity.CREATE);
        db.execSQL(DataBase.ProjectEntity.CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


}
