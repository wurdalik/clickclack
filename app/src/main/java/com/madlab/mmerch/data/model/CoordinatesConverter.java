package com.madlab.mmerch.data.model;

import androidx.annotation.NonNull;
import androidx.room.TypeConverter;

import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

public class CoordinatesConverter {

    @TypeConverter
    public String fromCoordinates(@NonNull LatLng coordinates){
        return String.format(Locale.getDefault(),"%f;%f", coordinates.longitude, coordinates.latitude);
    }

    @TypeConverter
    public LatLng toCoordinates(String data){
        String[]tmp = data.split(";");
        return new LatLng(Long.parseLong(tmp[0]), Long.parseLong(tmp[1]));
    }



}
