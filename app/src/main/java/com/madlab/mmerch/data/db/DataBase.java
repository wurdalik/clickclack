package com.madlab.mmerch.data.db;

import java.util.Locale;

public final class DataBase {

    public DataBase(){}

    public static abstract class ClientEntity{
        public static final String TABLE_NAME = "clients";
        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String PROJECTS = "projects";
        public static final String ADDRESS = "address";
        public static final String IS_ACTIVE = "is_active";
        public static final String COORDINATES_LATITUDE = "latitude";
        public static final String COORDINATES_LONGITUDE = "longitude";

        public static final String CREATE = String.format("CREATE TABLE %s(%s text PRIMARY KEY, %s text, %s text, %s text, %s integer, %s real, %s real)",
                TABLE_NAME,                       //
                ID,                               //
                TITLE,                            //
                PROJECTS,                         //
                ADDRESS,                          //
                IS_ACTIVE,                        //
                COORDINATES_LATITUDE,             //
                COORDINATES_LONGITUDE);           //
    }

    public static abstract class VisitEntity{
        public static final String TABLE_NAME = "visits";
        public static final String ID = "id";
        public static final String CLIENT_ID = "client_id";
        public static final String MANAGER_ID = "manager_id";
        public static final String PROJECTS = "projects";
        public static final String COINCIDENCE = "coincidence";
        public static final String ERROR = "error";
        public static final String DIFFERENCE = "difference";
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String COORDINATES_LATITUDE = "latitude";
        public static final String COORDINATES_LONGITUDE = "longitude";
        public static final String PATH_TO_PHOTO = "path_to_photo";

        public static final String CREATE = String.format("CREATE TABLE %s(%s text PRIMARY KEY, %s text, %s text, %s text, %s integer, %s real, %s real, %s integer, %s integer, %s real, %s real, %s text)",
                TABLE_NAME,
                ID,
                CLIENT_ID,
                MANAGER_ID,
                PROJECTS,
                COINCIDENCE,
                ERROR,
                DIFFERENCE,
                START_TIME,
                END_TIME,
                COORDINATES_LATITUDE,
                COORDINATES_LONGITUDE,
                PATH_TO_PHOTO);
    }

    public static abstract class ProjectEntity{
        public static final String TABLE_NAME = "projects";
        public static final String ID = "id";
        public static final String TITLE = "title";

        public static final String CREATE = String.format( "CREATE TABLE %s(%s PRIMARY KEY, %s)", TABLE_NAME, ID, TITLE);
    }

}
