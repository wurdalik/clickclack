package com.madlab.mmerch.data.model;

import java.util.ArrayList;

public class Client{
    private final String id;
    private final String address;
    private final String title;
    private final ArrayList<Project>projects;
    private final boolean isActive;
    private final double coordinates_latitude;
    private final double coordinates_longitude;

    public Client(String id, String address, String title, ArrayList<Project> projects, boolean isActive, double coordinates_latitude, double coordinates_longitude){
        this.id = id;
        this.address = address;
        this.title = title;
        this.projects = projects;
        this.isActive = isActive;
        this.coordinates_latitude = coordinates_latitude;
        this.coordinates_longitude = coordinates_longitude;
    }

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getTitle() {
        return title;
    }

    public double getCoordinates_latitude() {
        return coordinates_latitude;
    }

    public double getCoordinates_longitude() {
        return coordinates_longitude;
    }

    public boolean isActive() {
        return isActive;
    }

    public ArrayList<Project> getProjects() {
        return projects;
    }

    public String[] getProjectsArray(){
        if(this.getProjects().size()>0){
            String [] array = new String[this.getProjects().size()];
            for(int i = 0; i<=this.getProjects().size()-1; i++){
                array[i] = this.getProjects().get(i).getId();
            }
            return array;
        }
        else {
            return new String[]{};
        }
    }

    public String getProjectsString(){
        if(this.getProjects().size()>0){
            StringBuilder projects = new StringBuilder();
            for(Project project:this.projects){
                projects.append(project.getId())
                        .append(";");
            }
            return projects.toString();
        }
        else {
            return null;
        }

    }

}
