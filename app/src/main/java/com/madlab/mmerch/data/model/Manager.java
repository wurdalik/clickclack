package com.madlab.mmerch.data.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity
public class Manager {
    @PrimaryKey
    @SerializedName("code")
    private final String code;
    @SerializedName("title")
    private final String name;

    public Manager(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
