package com.madlab.mmerch.data.model;

import java.util.UUID;

public class Visit {
    private String id;
    private Client client;
    private String managerID;
    private String[] projects;
    private boolean coincidence;
    private double error;
    private double difference;
    private long startTime;
    private long endTime;
    private double coordinates_latitude;
    private double coordinates_longitude;
    private String photoPath;

    public Visit(){
        this.id = UUID.randomUUID().toString();
    }

    public Visit(String id, Client client, String managerID, String[] projects, boolean coincidence, double error, double difference, long startTime, long endTime, double coordinates_latitude, double coordinates_longitude, String photoPath) {
        this.id = id;
        this.client = client;
        this.managerID = managerID;
        this.projects = projects;
        this.coincidence = coincidence;
        this.error = error;
        this.difference = difference;
        this.startTime = startTime;
        this.endTime = endTime;
        this.coordinates_latitude = coordinates_latitude;
        this.coordinates_longitude = coordinates_longitude;
        this.photoPath = photoPath;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getManagerID() {
        return managerID;
    }

    public void setManagerID(String managerID) {
        this.managerID = managerID;
    }

    public String[] getProjects() {
        return projects;
    }

    public String ProjectsAsString(){
        if(this.getProjects().length>0){
            StringBuilder sb = new StringBuilder();
            for(int i=0; i<=this.getProjects().length-1;i++){
                sb.append(this.getProjects()[i])
                .append(";");
            }
            return sb.toString();
        }
        return null;
    }

    public void setProjects(String[] projects) {
        this.projects = projects;
    }

    public boolean isCoincidence() {
        return coincidence;
    }

    public void setCoincidence(boolean coincidence) {
        this.coincidence = coincidence;
    }

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = error;
    }

    public double getDifference() {
        return difference;
    }

    public void setDifference(double difference) {
        this.difference = difference;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double getCoordinates_latitude() {
        return coordinates_latitude;
    }

    public void setCoordinates_latitude(double coordinates_latitude) {
        this.coordinates_latitude = coordinates_latitude;
    }

    public double getCoordinates_longitude() {
        return coordinates_longitude;
    }

    public void setCoordinates_longitude(double coordinates_longitude) {
        this.coordinates_longitude = coordinates_longitude;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }
}
