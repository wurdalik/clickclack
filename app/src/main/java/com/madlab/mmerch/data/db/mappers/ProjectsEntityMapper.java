package com.madlab.mmerch.data.db.mappers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.madlab.mmerch.data.db.DataBase;
import com.madlab.mmerch.data.model.Project;

import java.util.ArrayList;

public class ProjectsEntityMapper {
    private static final String TAG = "ProjectsEntityMapper";

    public static Project getProjectByID(SQLiteDatabase database, String projectID){
        Project project = null;
        try(Cursor cursor = database.query(DataBase.ProjectEntity.TABLE_NAME, null, null, null, null, null, null)){
            if(cursor.getCount()>0){
                int id = cursor.getColumnIndex(DataBase.ProjectEntity.ID);
                int title = cursor.getColumnIndex(DataBase.ProjectEntity.TITLE);

                while (cursor.moveToNext()){
                    project = new Project(cursor.getString(id), cursor.getString(title));
                }
            }
        }
        catch (Exception e){
            Log.d(TAG, "getProjectByID: "+ e.getLocalizedMessage());
        }
        return project;
    }

    public static void insertProject(SQLiteDatabase database, ArrayList<Project>projects){
        for(Project project:projects){
            ContentValues contentValues = new ContentValues(2);
            contentValues.put(DataBase.ProjectEntity.ID, project.getId());
            contentValues.put(DataBase.ProjectEntity.TITLE, project.getTitle());
            database.insertWithOnConflict(DataBase.ProjectEntity.TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        }
    }

}
