package com.madlab.mmerch.data.model;

import android.location.Location;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity (tableName = "location")
public class LocationModel {
    @PrimaryKey
    private final String id;
    private final double latitude;
    private final double longitude;
    private final float accuracy;

    public LocationModel(String id, double latitude, double longitude, float accuracy){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.accuracy = accuracy;
    }

    @Ignore
    public LocationModel(Location location){
        id = UUID.randomUUID().toString();
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        accuracy = location.getAccuracy();
    }

    public Location getLocation(){
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setAccuracy(accuracy);
        return location;
    }

    public String getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getAccuracy() {
        return accuracy;
    }
}
