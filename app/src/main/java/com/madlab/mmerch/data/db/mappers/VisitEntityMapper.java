package com.madlab.mmerch.data.db.mappers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.madlab.mmerch.data.db.DataBase;
import com.madlab.mmerch.data.model.Visit;

import java.util.ArrayList;
import java.util.Arrays;

public class VisitEntityMapper {
    private static final String TAG = "VisitEntityMapper";

//    public static Visit getVisitByID(SQLiteDatabase database, String visitID){
//        Visit visit = null;
//        try(Cursor cursor = database.query(DataBase.VisitEntity.TABLE_NAME, null, "id = ?", new String[]{visitID}, null, null, null)){
//            if(cursor.getCount()>0){
//                while (cursor.moveToNext()){
//                    int id = cursor.getColumnIndex(DataBase.VisitEntity.ID);
//                    int clientID = cursor.getColumnIndex(DataBase.VisitEntity.CLIENT_ID);
//                    int managerID = cursor.getColumnIndex(DataBase.VisitEntity.MANAGER_ID);
//                    int projects = cursor.getColumnIndex(DataBase.VisitEntity.PROJECTS);
//                    int coincidence = cursor.getColumnIndex(DataBase.VisitEntity.COINCIDENCE);
//                    int error = cursor.getColumnIndex(DataBase.VisitEntity.ERROR);
//                    int difference = cursor.getColumnIndex(DataBase.VisitEntity.DIFFERENCE);
//                    int startTime = cursor.getColumnIndex(DataBase.VisitEntity.START_TIME);
//                    int endTime = cursor.getColumnIndex(DataBase.VisitEntity.END_TIME);
//                    int coordinates_latitude = cursor.getColumnIndex(DataBase.VisitEntity.COORDINATES_LATITUDE);
//                    int coordinates_longitude = cursor.getColumnIndex(DataBase.VisitEntity.COORDINATES_LONGITUDE);
//
//
//                    visit = new Visit(
//                            cursor.getString(id),
//                            ClientEntityMapper.getClientByID(database, cursor.getString(clientID)),
//                            cursor.getString(managerID),
//                            cursor.getString(projects).split(","),
//                            cursor.getInt(coincidence)==1,
//                            cursor.getDouble(error),
//                            cursor.getDouble(difference),
//                            cursor.getLong(startTime),
//                            cursor.getLong(endTime),
//                            cursor.getDouble(coordinates_latitude),
//                            cursor.getDouble(coordinates_longitude)
//                    );
//                }
//            }
//        }
//        catch (SQLException e){
//            Log.d(TAG, "getVisitbyID: "+ e.getLocalizedMessage());
//        }
//        return visit;
//    }

    public static ArrayList<Visit> getAllVisits(SQLiteDatabase database){
        ArrayList<Visit> visits = new ArrayList<>();
        try(Cursor cursor = database.query(DataBase.VisitEntity.TABLE_NAME, null, null, null, null, null, null)){
            if(cursor.getCount()>0){
                int id = cursor.getColumnIndex(DataBase.VisitEntity.ID);
                int clientID = cursor.getColumnIndex(DataBase.VisitEntity.CLIENT_ID);
                int managerID = cursor.getColumnIndex(DataBase.VisitEntity.MANAGER_ID);
                int projects = cursor.getColumnIndex(DataBase.VisitEntity.PROJECTS);
                int coincidence = cursor.getColumnIndex(DataBase.VisitEntity.COINCIDENCE);
                int error = cursor.getColumnIndex(DataBase.VisitEntity.ERROR);
                int difference = cursor.getColumnIndex(DataBase.VisitEntity.DIFFERENCE);
                int startTime = cursor.getColumnIndex(DataBase.VisitEntity.START_TIME);
                int endTime = cursor.getColumnIndex(DataBase.VisitEntity.END_TIME);
                int coordinates_latitude = cursor.getColumnIndex(DataBase.VisitEntity.COORDINATES_LATITUDE);
                int coordinates_longitude = cursor.getColumnIndex(DataBase.VisitEntity.COORDINATES_LONGITUDE);
                int photoPath = cursor.getColumnIndex(DataBase.VisitEntity.PATH_TO_PHOTO);

                Visit visit = new Visit(
                        cursor.getString(id),
                        ClientEntityMapper.getClientByID(database, cursor.getString(clientID)),
                        cursor.getString(managerID),
                        cursor.getString(projects).split(","),
                        cursor.getInt(coincidence)==1,
                        cursor.getDouble(error),
                        cursor.getDouble(difference),
                        cursor.getLong(startTime),
                        cursor.getLong(endTime),
                        cursor.getDouble(coordinates_latitude),
                        cursor.getDouble(coordinates_longitude),
                        cursor.getString(photoPath)
                );
                visits.add(visit);
            }
        }
        catch (SQLException e){
            Log.d(TAG, "getAllVisits: "+ e.getLocalizedMessage());
        }
        return visits;
    }

    public static void insert(SQLiteDatabase database, Visit visit){
        database.beginTransaction();
        try{
            ContentValues contentValues = new ContentValues(12);
            contentValues.put(DataBase.VisitEntity.ID, visit.getId());
            contentValues.put(DataBase.VisitEntity.CLIENT_ID, visit.getClient().getId());
            contentValues.put(DataBase.VisitEntity.MANAGER_ID, visit.getManagerID());
            contentValues.put(DataBase.VisitEntity.PROJECTS, visit.ProjectsAsString());
            contentValues.put(DataBase.VisitEntity.COINCIDENCE, visit.isCoincidence()?1:0);
            contentValues.put(DataBase.VisitEntity.ERROR, visit.getError());
            contentValues.put(DataBase.VisitEntity.DIFFERENCE, visit.getDifference());
            contentValues.put(DataBase.VisitEntity.START_TIME, visit.getStartTime());
            contentValues.put(DataBase.VisitEntity.END_TIME, visit.getEndTime());
            contentValues.put(DataBase.VisitEntity.COORDINATES_LATITUDE, visit.getCoordinates_latitude());
            contentValues.put(DataBase.VisitEntity.COORDINATES_LONGITUDE, visit.getCoordinates_longitude());
            contentValues.put(DataBase.VisitEntity.PATH_TO_PHOTO, visit.getPhotoPath());

            database.insert(DataBase.VisitEntity.TABLE_NAME, null, contentValues);
            database.setTransactionSuccessful();
        }
        catch (SQLException e){
            Log.d(TAG, "insert: "+ e.getLocalizedMessage());
        }
        finally {
            database.endTransaction();
        }

    }




}
