package com.madlab.mmerch.data.model;

public class Product {
    private String code;
    private String name;
    private boolean isChecked;

    public Product(String code, String name, boolean isChecked){
        this.code = code;
        this.name = name;
        this.isChecked = isChecked;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
