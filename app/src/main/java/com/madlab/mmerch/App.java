package com.madlab.mmerch;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.madlab.mmerch.data.db.DataBaseHelper;
import com.madlab.mmerch.prefs.AppPreferencesHelper;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.prefs.PrefsConst;
import com.patloew.rxlocation.RxLocation;

import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

public class App extends Application {
    private SQLiteDatabase database;
    private PreferencesHelper preference;
    private RxLocation location;

    private CustomInjector injector;
    @Override
    public void onCreate() {
        super.onCreate();
        database = new DataBaseHelper(this).getWritableDatabase();
        preference = AppPreferencesHelper.get(this, PrefsConst.PREFERENCES_NAME);
        location = new RxLocation(this);

        registerActivityLifecycleCallbacks(new CustomInjector(database, preference, location));
        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                String s = "123";
            }
        });
    }

}
