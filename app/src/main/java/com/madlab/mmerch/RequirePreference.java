package com.madlab.mmerch;

import com.madlab.mmerch.prefs.PreferencesHelper;

public interface RequirePreference {
    void setPreference(PreferencesHelper preference);
}
