package com.madlab.mmerch.common;

import androidx.fragment.app.Fragment;

import moxy.MvpView;

public interface BaseView extends MvpView {

    void showLoading();

    void hideLoading();



    //fixme for checked token
//    void openLoginFragment();

    void showError();

    void showMessage(String message);

    void replaceFragment(Fragment fragment);
}
