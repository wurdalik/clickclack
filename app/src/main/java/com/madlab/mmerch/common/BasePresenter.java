package com.madlab.mmerch.common;

public interface BasePresenter {
    void subscribe();

    void unsubscribe();

    void setFragment();
    
}
