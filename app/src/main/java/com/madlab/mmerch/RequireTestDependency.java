package com.madlab.mmerch;

import android.database.sqlite.SQLiteDatabase;

import com.madlab.mmerch.prefs.PreferencesHelper;
import com.patloew.rxlocation.RxLocation;

public interface RequireTestDependency {
    void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location);
}
