package com.madlab.mmerch.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

public class AppPreferencesHelper implements PreferencesHelper {
    private static AppPreferencesHelper preferencesHelper;
    private final SharedPreferences mPrefs;

    public static AppPreferencesHelper get(Context context, String prefFileName){
        if(preferencesHelper==null){
            preferencesHelper = new AppPreferencesHelper(context, prefFileName);
        }
        return preferencesHelper;
    }

    private AppPreferencesHelper(Context context, String prefFileName){
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public void saveToken(String token) {
        byte[] data = token.getBytes();
        String encode = Base64.encodeToString(data, Base64.DEFAULT);
        mPrefs.edit()
                .putString(PrefsConst.TOKEN, encode)
                .apply();
    }

    @Override
    public String getToken() {
        String tmp = mPrefs.getString(PrefsConst.TOKEN, null);
        if (tmp!=null){
            byte[] tmp2 = Base64.decode(tmp, Base64.DEFAULT);
            String result = new String(tmp2);
            return result;
        }
        else {
            return null;
        }
    }

    @Override
    public void saveCode(String userCode) {
        mPrefs.edit()
                .putString("code", userCode)
                .apply();
    }

    @Override
    public String getUserCode(){
        return mPrefs.getString("code", "");
    }

    @Override
    public void saveFIO(String fio) {
        mPrefs.edit()
                .putString("fio", fio)
                .apply();
    }

    @Override
    public String getFIO() {
        return mPrefs.getString("fio", "");
    }

    @Override
    public void clearSettings() {
        mPrefs.edit().clear().apply();
    }
}
