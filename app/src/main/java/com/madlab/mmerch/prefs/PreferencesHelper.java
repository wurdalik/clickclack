package com.madlab.mmerch.prefs;

public interface PreferencesHelper {

    void saveToken(String token);

    String getToken();

    void saveCode(String userCode);

    String getUserCode();

    void saveFIO(String fio);

    String getFIO();

    void clearSettings();
}
