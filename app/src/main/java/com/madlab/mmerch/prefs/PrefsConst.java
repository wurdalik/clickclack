package com.madlab.mmerch.prefs;

public class PrefsConst {
    public static final String PREFERENCES_NAME = "settings";
    public static final String TOKEN = "token";
}
