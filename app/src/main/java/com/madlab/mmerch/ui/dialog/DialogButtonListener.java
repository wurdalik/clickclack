package com.madlab.mmerch.ui.dialog;

public interface DialogButtonListener {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}
