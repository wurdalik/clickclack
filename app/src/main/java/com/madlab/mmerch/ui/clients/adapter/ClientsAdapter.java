package com.madlab.mmerch.ui.clients.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.data.model.Client;

import java.util.ArrayList;
import java.util.List;

public class ClientsAdapter extends RecyclerView.Adapter<ClientViewHolder> {
    private final ArrayList<Client>clients = new ArrayList<>();

    @NonNull
    @Override
    public ClientViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ClientViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientViewHolder holder, int position) {
        holder.bind(clients.get(position));
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    public void setData(List<Client> list){
        clients.clear();
        clients.addAll(list);
        notifyDataSetChanged();
    }

    public String getClientId(int position){
        if(!clients.isEmpty() && position<=clients.size()-1){
            return clients.get(position).getId();
        }
        return null;
    }

}
