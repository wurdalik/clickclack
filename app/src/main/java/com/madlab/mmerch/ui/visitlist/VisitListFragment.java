package com.madlab.mmerch.ui.visitlist;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.madlab.mmerch.RequireTestDependency;
import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BaseFragment;
import com.madlab.mmerch.ui.base.BasePresenter;
import com.patloew.rxlocation.RxLocation;

import java.util.List;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

public class VisitListFragment extends BaseFragment implements VisitListView, RequireTestDependency {
    private SQLiteDatabase database;
    private PreferencesHelper preferences;

    @InjectPresenter
    VisitListPresenter presenter;

    @ProvidePresenter
    VisitListPresenter providePresenter(){
        return new VisitListPresenter(database, preferences);
    }

    public static VisitListFragment newInstance() {
        VisitListFragment fragment = new VisitListFragment();
        return fragment;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getData();
    }

    @Override
    public void setData(List<Visit> visits) {

    }

    @Override
    public void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location) {
        this.database = dataBase;
        this.preferences = preferences;
    }
}
