package com.madlab.mmerch.ui.clients;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.ItemClickSupport;
import com.madlab.mmerch.PermissionHandler;
import com.madlab.mmerch.R;
import com.madlab.mmerch.RequireTestDependency;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.MainActivity;
import com.madlab.mmerch.ui.base.BaseFragment;
import com.madlab.mmerch.ui.base.BasePresenter;
import com.madlab.mmerch.ui.clients.adapter.ClientsAdapter;
import com.patloew.rxlocation.RxLocation;

import java.util.ArrayList;
import java.util.List;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

/**
 * Created by Toxo on 17.11.2019
 */


public class TodayClientsFragment extends BaseFragment implements TodayClientsView, ItemClickSupport.OnItemClickListener, PermissionHandler, RequireTestDependency, UpdateData{
    private Context context;
    private SQLiteDatabase database;
    private PreferencesHelper preferences;
    private RecyclerView recycler;
    private ClientsAdapter adapter;

    @InjectPresenter
    TodayClientsPresenter presenter;

    @ProvidePresenter
    TodayClientsPresenter providePresenter(){
        return new TodayClientsPresenter(database, preferences);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.today_clients, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ClientsAdapter();
        recycler = view.findViewById(R.id.today_recycler);
        recycler.setLayoutManager(new LinearLayoutManager(context));
        recycler.addItemDecoration(new DividerItemDecoration(recycler.getContext(), DividerItemDecoration.VERTICAL));
        recycler.setAdapter(adapter);

        ItemClickSupport.addTo(recycler).setOnItemClickListener(this);

        presenter.getClients().observe(this, new Observer<ArrayList<Client>>() {
            @Override
            public void onChanged(ArrayList<Client> clients) {
                adapter.setData(clients);
            }
        });
    }

    @Override
    public void addData(List<Client> clients) {
        adapter.setData(clients);
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
        if(checkHasPermission()){
            presenter.startVisit(adapter.getClientId(position));
        }
        else{
            requestPermission();
        }
    }

    @Override
    public boolean checkHasPermission() {
        if(ContextCompat.checkSelfPermission((MainActivity)context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return ContextCompat.checkSelfPermission((MainActivity)context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }
        return false;
    }

    @Override
    public void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((MainActivity)context).requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION}, 101);
        }
    }

    @Override
    public void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location) {
        this.database = dataBase;
        this.preferences = preferences;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        ((MainActivity)getActivity()).changeFragment(fragment, true);
    }

    @Override
    public void updateData() {
        presenter.updateClients();
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }
}
