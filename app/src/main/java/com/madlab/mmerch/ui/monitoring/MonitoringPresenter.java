package com.madlab.mmerch.ui.monitoring;

import com.madlab.mmerch.data.model.Product;

import java.util.ArrayList;

public class MonitoringPresenter implements MonitoringContract.Presenter{
    private MonitoringContract.View view;

    public MonitoringPresenter(MonitoringContract.View view){
        this.view = view;
    }


    @Override
    public void getData() {
        ArrayList<Product>products = new ArrayList<>();
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        products.add(new Product("", "Тестовый продукт", false));
        view.addData(products);
    }

    @Override
    public void subscribe() {
        getData();
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void setFragment() {

    }
}
