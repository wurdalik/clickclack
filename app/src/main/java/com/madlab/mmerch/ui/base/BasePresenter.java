package com.madlab.mmerch.ui.base;

import io.reactivex.disposables.CompositeDisposable;
import moxy.MvpPresenter;
import moxy.MvpView;

public abstract class BasePresenter<V extends MvpView> extends MvpPresenter<V> {
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void disposeAll(){
        compositeDisposable.dispose();
    }
}
