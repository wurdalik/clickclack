package com.madlab.mmerch.ui.projects;

import com.madlab.mmerch.common.BaseView;
import com.madlab.mmerch.data.model.Project;

import java.util.ArrayList;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface ProjectsView extends BaseView {
    void addData(ArrayList<Project>projects);
}
