package com.madlab.mmerch.ui.visit;

import androidx.fragment.app.Fragment;

import com.madlab.mmerch.data.model.Project;

import java.util.ArrayList;

import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.AddToEndStrategy;
import moxy.viewstate.strategy.SkipStrategy;
import moxy.viewstate.strategy.StateStrategyType;
@StateStrategyType(AddToEndSingleStrategy.class)
public interface VisitView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void showMessage(String message);
    void setClientTitle(String title);
    void setClientAddress(String address);
    void setLocation(String location);
    void showDistance(Double distance);
    void hideLoading();
    void showLoading();
    void replaceFragment(Fragment fragment);
    void showLocationRepeat();
    void hideLocationRepeat();
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setChildfragment(Fragment fragment);

    void sendPhoto();

    void addProjects(ArrayList<Project> projects);
    void close();
}
