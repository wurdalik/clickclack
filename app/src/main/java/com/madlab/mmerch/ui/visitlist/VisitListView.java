package com.madlab.mmerch.ui.visitlist;

import com.madlab.mmerch.data.model.Visit;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface VisitListView extends MvpView {
    void setData(List<Visit> visits);
}
