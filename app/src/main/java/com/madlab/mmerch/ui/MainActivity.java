package com.madlab.mmerch.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.madlab.mmerch.R;
import com.madlab.mmerch.prefs.AppPreferencesHelper;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.prefs.PrefsConst;
import com.madlab.mmerch.ui.clients.ListOfClientsFragment;
import com.madlab.mmerch.ui.dialog.CustomDialogs;
import com.madlab.mmerch.ui.dialog.DialogButtonListener;
import com.madlab.mmerch.ui.login.LoginFragment;
import com.madlab.mmerch.ui.menu.MenuFragment;
import com.madlab.mmerch.ui.visit.VisitFragment;

public class MainActivity extends AppCompatActivity implements DialogButtonListener {
    private String visit;
    private CustomDialogs customDialogs;
    private PreferencesHelper preferencesHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preferencesHelper = AppPreferencesHelper.get(this, PrefsConst.PREFERENCES_NAME);

        if(savedInstanceState==null){
            if(preferencesHelper.getToken()!=null){
                changeFragment(MenuFragment.newInstance(), false);
            }
            else {
                changeFragment(LoginFragment.newInstance(), false);
            }
        }
    }

    public void changeFragment(Fragment fragment, boolean addStack){
//        boolean addToStack = getSupportFragmentManager().findFragmentById(R.id.fragment_container)!=null;

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment);
        if(addStack){
            fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        fragmentTransaction.commit();
    }

    public void showLoading(){
//        customDialogs = new CustomDialogs();
//        customDialogs.show(this.getSupportFragmentManager(), null);
        //        progress = builder.create();
//        progress.show();
    }

    public void hideLoading(){
        if(customDialogs!=null&&customDialogs.isResumed()){
            customDialogs.dismiss();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPositiveButtonClick() {
        changeFragment(VisitFragment.newInstance(visit), true);
    }

    @Override
    public void onNegativeButtonClick() {
        finish();
    }
}
