package com.madlab.mmerch.ui.monitoring.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.R;
import com.madlab.mmerch.data.model.Product;

class ProductHolder extends RecyclerView.ViewHolder {
    private TextView name;
    private CheckBox checkBox;

    public ProductHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.product_item, parent, false));
        name = itemView.findViewById(R.id.product_name);
        checkBox = itemView.findViewById(R.id.product_checked);
    }

    public void bindProduct(Product product){
        name.setText(product.getName());
        checkBox.setChecked(product.isChecked());
    }



}
