package com.madlab.mmerch.ui.projects;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.R;
import com.madlab.mmerch.data.model.Project;

import java.util.ArrayList;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ProjectsViewHolder> {
    private final ArrayList<Project> projects = new ArrayList<>();

    @NonNull
    @Override
    public ProjectsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProjectsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.projects_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectsViewHolder holder, int position) {
        holder.bindProject(projects.get(position));
    }



    @Override
    public int getItemCount() {
        return projects.size();
    }

    public void addData(ArrayList<Project>projects){
        this.projects.clear();
        this.projects.addAll(projects);
        notifyDataSetChanged();
    }

    public class ProjectsViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ProjectsViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.project_title);
        }

        public void bindProject(Project project){
            title.setText(project.getTitle());
        }
    }
}
