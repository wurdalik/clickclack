package com.madlab.mmerch.ui.cabinet;

import com.madlab.mmerch.common.BasePresenter;
import com.madlab.mmerch.common.BaseView;

public interface CabinetContract {
    interface View extends BaseView {

        void logOut();
    }

    interface Presenter extends BasePresenter{

    }
}
