//package com.madlab.clickclack.ui.visit;
//
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.madlab.clickclack.data.model.Client;
//import com.madlab.clickclack.data.model.Task;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {
//        private final ArrayList<Task> tasks = new ArrayList<>();
//
//        @NonNull
//        @Override
//        public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            return new TaskViewHolder(LayoutInflater.from(parent.getContext()), parent);
//        }
//
//        @Override
//        public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
//            holder.bind(tasks.get(position));
//        }
//
//        @Override
//        public int getItemCount() {
//            return tasks.size();
//        }
//
//        public void setData(List<Task> list){
//            tasks.clear();
//            tasks.addAll(list);
//            notifyDataSetChanged();
//        }
//
//        public String getClientID(int position){
//            if(!tasks.isEmpty() && position<= tasks.size()-1){
//                return tasks.get(position).getCode();
//            }
//            return null;
//        }
//    }
