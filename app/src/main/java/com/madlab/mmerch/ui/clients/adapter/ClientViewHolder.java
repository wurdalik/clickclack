package com.madlab.mmerch.ui.clients.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.R;
import com.madlab.mmerch.data.model.Client;

class ClientViewHolder extends RecyclerView.ViewHolder {
    public TextView name;
    public TextView address;


    public ClientViewHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.adapter_item_client, parent, false));

        name = itemView.findViewById(R.id.name);
        address = itemView.findViewById(R.id.address);
    }

    public void bind(Client client){
        name.setText(client.getTitle());
        address.setText(client.getAddress());
    }
}
