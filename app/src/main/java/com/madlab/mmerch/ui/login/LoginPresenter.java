package com.madlab.mmerch.ui.login;

import android.text.TextUtils;
import android.util.Patterns;

import com.google.gson.JsonObject;
import com.madlab.mmerch.api.ApiUtils;
import com.madlab.mmerch.data.LoginRequest;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.clients.ListOfClientsFragment;
import com.madlab.mmerch.ui.menu.MenuFragment;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter implements LoginContract.Presenter {
    private LoginContract.LoginView view;
    private PreferencesHelper preferencesHelper;
    private CompositeDisposable mCompositeDisposable;

    public LoginPresenter(LoginContract.LoginView view, PreferencesHelper preferencesHelper){
        this.view = view;
        this.preferencesHelper = preferencesHelper;
        mCompositeDisposable = new CompositeDisposable();
        view.setPresenter(this);
    }

    @Override
    public void signIn(String login) {
        if(isEmailValid(login)){
                LoginRequest.ServerLoginRequest serverLoginRequest = new LoginRequest.ServerLoginRequest(login);
                userlogin(serverLoginRequest);
        }
        else{
            view.loginError();
        }
    }

    private void userlogin(LoginRequest.ServerLoginRequest loginRequest){
        view.showLoading();

        mCompositeDisposable.add(
                ApiUtils.getSiteApi()
                        .doServerLoginApiCall(loginRequest.getJson())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doFinally(()-> view.hideLoading())
                        .subscribe(new Consumer<JsonObject>() {
                            @Override
                            public void accept(JsonObject jsonObject) throws Exception {
                                view.showMessage("Добро пожаловать "+ jsonObject.get("title").getAsString());
                                preferencesHelper.saveFIO(jsonObject.get("title").getAsString());
                                preferencesHelper.saveCode(loginRequest.getUserCode());
                                preferencesHelper.saveToken(jsonObject.get("token").getAsString());
                                view.replaceFragment(MenuFragment.newInstance());
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                view.hideLoading();
                                view.showMessage(throwable.getMessage());
//                                view.replaceFragment(MenuFragment.newInstance());
                            }
                        })
        );
    }



    //fixme chang this
    private void alternativeLogin(String userCode){
//        mCompositeDisposable.add(
//                ApiUtils.getAlternativeApi()
//                        .doServerLoginApiCall(loginRequest.getJson())
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(jsonObject -> {
//                            preferencesHelper.saveToken(jsonObject.get("serverLoginRequest").getAsJsonObject().get("token").getAsString());
//                        }, throwable -> {
//                            view.showMessage(throwable.getMessage());
//                            view.replaceFragment(MenuFragment.newInstance());
//                        })
//        );
    }



    private boolean isEmailValid(String email){
        return email!=null
                &&!TextUtils.isEmpty(email);
    }

    private boolean isPasswordValid(String password){
        return password!=null
                && !TextUtils.isEmpty(password)
                && password.length()>1; //fixme поправить проверку
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {
        mCompositeDisposable.clear();
    }

    @Override
    public void setFragment() {

    }

}
