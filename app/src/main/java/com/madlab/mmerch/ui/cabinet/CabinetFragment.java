package com.madlab.mmerch.ui.cabinet;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.madlab.mmerch.R;
import com.madlab.mmerch.prefs.AppPreferencesHelper;
import com.madlab.mmerch.prefs.PrefsConst;
import com.madlab.mmerch.ui.MainActivity;

public class CabinetFragment extends Fragment implements CabinetContract.View, View.OnClickListener {
    private Context context;
    private CabinetPresenter presenter;
    private AppPreferencesHelper appPreferencesHelper;
    private Button logout;


    public static CabinetFragment newInstance() {
        CabinetFragment fragment = new CabinetFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferencesHelper = AppPreferencesHelper.get(context, PrefsConst.PREFERENCES_NAME);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cabinet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        logout = view.findViewById(R.id.sign_out);
        logout.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new CabinetPresenter(this, appPreferencesHelper);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sign_out:
                presenter.logout();
                break;
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {

    }


    @Override
    public void replaceFragment(Fragment fragment) {

    }

    @Override
    public void logOut() {
        ((MainActivity)context).getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
