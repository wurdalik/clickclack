package com.madlab.mmerch.ui.clients;

import android.database.sqlite.SQLiteDatabase;

import androidx.lifecycle.MutableLiveData;

import com.madlab.mmerch.api.ApiUtils;
import com.madlab.mmerch.data.db.mappers.ClientEntityMapper;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Clients;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BasePresenter;
import com.madlab.mmerch.ui.visit.VisitFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;

/**
 * Created by Toxo on 17.11.2019
 */

@InjectViewState
public class TodayClientsPresenter extends BasePresenter<TodayClientsView> {
    private SQLiteDatabase database;
    private PreferencesHelper preferences;
    private MutableLiveData<ArrayList<Client>> clients = new MutableLiveData<>();

    public TodayClientsPresenter(SQLiteDatabase database, PreferencesHelper preferences){
        this.database = database;
        this.preferences = preferences;

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        clients.postValue(ClientEntityMapper.getActiveClients(database));
    }

    public void startVisit(String codeClient) {
//        compositeDisposable.add(
//                Completable.fromAction(()-> VisitEntityMapper.insert(database, visit))
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnError(new Consumer<Throwable>() {
//                            @Override
//                            public void accept(Throwable throwable) throws Exception {
//                                getViewState().showMessage(throwable.getMessage());
//                            }
//                        })
//                        .subscribe());
        if(ClientEntityMapper.haveProjects(database, codeClient)){
            getViewState().replaceFragment(VisitFragment.newInstance(codeClient));
        }
        else{
            getViewState().showMessage("У данного клиента нет проектов!");
        }

    }

    public void updateClients() {
        String token = "Token "+preferences.getToken();
        String code = preferences.getUserCode();

        compositeDisposable.add(
                ApiUtils.getSiteApi().getClients(token, code)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doFinally(()->getViewState().hideLoading())
                        .subscribe(new Consumer<Clients>() {
                                       @Override
                                       public void accept(Clients clients) throws Exception {
                                           saveToDB(clients.getClients());
                                       }
                                   },
                                new Consumer<Throwable>() {
                                    @Override
                                    public void accept(Throwable throwable) throws Exception {
                                        getViewState().showError();
                                        getViewState().showMessage(throwable.getMessage());
                                    }
                                })
        );
    }

    public void getDataFromDB(){
        compositeDisposable.add(
                Observable.just(ClientEntityMapper.getAllClients(database))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(clients -> getViewState().addData(clients),
                                throwable -> getViewState().showMessage(throwable.getMessage()))
        );
    }


    public void saveToDB(List<Client> clients){
        compositeDisposable.add(
                Completable.fromAction(()->ClientEntityMapper.saveClients(database, clients))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action() {
                                       @Override
                                       public void run() throws Exception {
                                           TodayClientsPresenter.this.clients.postValue(ClientEntityMapper.getActiveClients(database));
                                       }
                                   },
                                throwable -> getViewState().showError()));
    }

    public MutableLiveData<ArrayList<Client>> getClients() {
        return clients;
    }
}
