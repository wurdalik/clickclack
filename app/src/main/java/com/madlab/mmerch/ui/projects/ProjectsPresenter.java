package com.madlab.mmerch.ui.projects;

import android.database.sqlite.SQLiteDatabase;

import com.madlab.mmerch.data.db.mappers.ClientEntityMapper;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BasePresenter;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;

@InjectViewState
public class ProjectsPresenter extends BasePresenter<ProjectsView> {
    private SQLiteDatabase database;
    private PreferencesHelper preferences;
    private String clientID;

    public ProjectsPresenter(SQLiteDatabase database, PreferencesHelper preferences, String clientID){
        this.database = database;
        this.preferences = preferences;
        this.clientID = clientID;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getProjectsByClientID(clientID);
    }

    private void getProjectsByClientID(String clientID){
        compositeDisposable.add(
                Observable.just(ClientEntityMapper.getClientByID(database, clientID))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> getViewState().showMessage(throwable.getMessage()))
                .subscribe(client -> getViewState().addData(client.getProjects()))
        );
    }

}
