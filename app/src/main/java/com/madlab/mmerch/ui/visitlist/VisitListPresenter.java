package com.madlab.mmerch.ui.visitlist;

import android.database.sqlite.SQLiteDatabase;

import com.madlab.mmerch.data.db.mappers.VisitEntityMapper;
import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BasePresenter;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;

@InjectViewState
public class VisitListPresenter extends BasePresenter<VisitListView> {

    private SQLiteDatabase dataBase;
    private PreferencesHelper preferences;

    public VisitListPresenter(SQLiteDatabase database, PreferencesHelper preferences){
        this.dataBase = database;
        this.preferences = preferences;
    }

    public void getData(){
        compositeDisposable.add(
                Single.just(VisitEntityMapper.getAllVisits(dataBase))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<List<Visit>>() {
                            @Override
                            public void accept(List<Visit> visits) throws Exception {
                                getViewState().setData(visits);
                            }
                        })
        );
    }

}
