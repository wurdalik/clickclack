package com.madlab.mmerch.ui.login;

import com.madlab.mmerch.common.BasePresenter;
import com.madlab.mmerch.common.BaseView;

public interface LoginContract {

    interface LoginView extends BaseView {

        void setPresenter(LoginPresenter presenter);

        void loginError();

        void passwordError();

        void showMessage(String message);

    }

    interface Presenter extends BasePresenter{

        void signIn(String login);

    }

}
