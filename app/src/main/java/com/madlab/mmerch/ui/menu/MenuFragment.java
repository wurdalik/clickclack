package com.madlab.mmerch.ui.menu;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.madlab.mmerch.R;
import com.madlab.mmerch.RequireTestDependency;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.MainActivity;
import com.madlab.mmerch.ui.cabinet.CabinetFragment;
import com.madlab.mmerch.ui.clients.AllClientsFragment;
import com.madlab.mmerch.ui.clients.ListOfClientsFragment;
import com.madlab.mmerch.ui.clients.UpdateData;
import com.madlab.mmerch.ui.login.LoginFragment;
import com.madlab.mmerch.ui.visit.VisitFragment;
import com.patloew.rxlocation.RxLocation;

public class MenuFragment extends Fragment implements MenuContract.View, NavigationView.OnNavigationItemSelectedListener, Toolbar.OnMenuItemClickListener, RequireTestDependency {
    private MenuPresenter presenter;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private Toolbar toolbar;

    private TextView fio;

    private Context context;

    private PreferencesHelper preferences;

    private ListOfClientsFragment listOfClientsFragment = ListOfClientsFragment.newInstance();
    private CabinetFragment cabinetFragment = CabinetFragment.newInstance();
    //private VisitFragment visitFragment = VisitFragment.newInstance()

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        getChildFragmentManager().beginTransaction().replace(R.id.menu_fragment_container, listOfClientsFragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        toolbar = view.findViewById(R.id.menu_fragment_toolbar);
        navigationView = view.findViewById(R.id.menu_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        drawerLayout = view.findViewById(R.id.menu_drawer);

        toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (slideOffset == 1.0f) {
                    fio = drawerView.findViewById(R.id.fio);
                    fio.setText(preferences.getFIO());
                }
            }
        };
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        toolbar.setTitle("Маршрут посещения тт");
        toolbar.inflateMenu(R.menu.clients_fragment_menu);
        toolbar.setOnMenuItemClickListener(this);
        presenter = new MenuPresenter(this);
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        switch (menuItem.getItemId()){
            case R.id.clients_route:
                toolbar.setTitle("Маршрут посещения тт");
                getChildFragmentManager().beginTransaction().replace(R.id.menu_fragment_container, listOfClientsFragment).commit();
                break;
            case R.id.exit:
                fragmentManager.beginTransaction().replace(R.id.fragment_container, LoginFragment.newInstance()).commit();
//
//            case R.id.visit:
//                toolbar.setTitle("Новый визит");
//                fragmentManager.beginTransaction().replace(R.id.menu_fragment_container, listOfClientsFragment).addToBackStack(null).commit();
//
//            case R.id.settings:
//                toolbar.setTitle("Настройки");
//                fragmentManager.beginTransaction().replace(R.id.menu_fragment_container, cabinetFragment).addToBackStack(null).commit();
//                break;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {

    }


    @Override
    public void replaceFragment(Fragment fragment) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.update_data:
                ((ListOfClientsFragment)getChildFragmentManager().getFragments().get(0)).Update();
                break;
        }
        return false;
    }

    @Override
    public void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location) {
        this.preferences = preferences;
    }
}
