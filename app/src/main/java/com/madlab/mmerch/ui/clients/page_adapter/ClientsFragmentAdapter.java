package com.madlab.mmerch.ui.clients.page_adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.madlab.mmerch.ui.clients.TodayClientsFragment;
import com.madlab.mmerch.ui.clients.UpdateData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Toxo on 17.11.2019
 */


public class ClientsFragmentAdapter extends FragmentStateAdapter {
    private final List<Fragment>fragmentList = new ArrayList<>();
    private final List<String>fragmentTitleList = new ArrayList<>();

    public ClientsFragmentAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public void addFragment(Fragment fragment, String fragmentTitle){
        fragmentList.add(fragment);
        fragmentTitleList.add(fragmentTitle);
        notifyDataSetChanged();
    }

    public String getTabTitle(int position){
        return fragmentTitleList.get(position);
    }

    public void updateData(){
//        for(Fragment fragment:fragmentList){
//            ((UpdateData)fragment).updateData();
//        }

        ((UpdateData)fragmentList.get(0)).updateData();

    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {

        return fragmentList.get(position);
    }

    @Override
    public int getItemCount() {
        return 2;
    }


}
