package com.madlab.mmerch.ui.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.button.MaterialButton;
import com.madlab.mmerch.R;
import com.madlab.mmerch.prefs.AppPreferencesHelper;
import com.madlab.mmerch.prefs.PrefsConst;
import com.madlab.mmerch.ui.MainActivity;

public class LoginFragment extends Fragment implements LoginContract.LoginView, View.OnClickListener {
    private LoginPresenter mPresenter;
    private Context context;

    private ProgressBar progressBar;

    private MaterialButton mButtonSignIn;
    private EditText mEditTextLogin;
    private EditText mEditTextPassword;

    //fixme only for demonstrarions
    private TextView test;
    private AppPreferencesHelper preferencesHelper;

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesHelper = AppPreferencesHelper.get(context, PrefsConst.PREFERENCES_NAME);
        preferencesHelper.clearSettings();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_temp, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mButtonSignIn = view.findViewById(R.id.button_sign_in);
        mButtonSignIn.setOnClickListener(this);

        String token = preferencesHelper.getToken();

        progressBar = view.findViewById(R.id.login_pb);

        progressBar.setIndeterminate(true);

        mEditTextLogin = view.findViewById(R.id.edit_text_login);

        //fixme only for testing!
        test = view.findViewById(R.id.test);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new LoginPresenter(this, preferencesHelper);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_sign_in:
                mPresenter.signIn(mEditTextLogin.getText().toString());
                break;
            case R.id.test:
                mEditTextLogin.setText("С11316");
                break;
        }
    }

    @Override
    public void setPresenter(LoginPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void loginError(){
        mEditTextLogin.requestFocus();
        mEditTextLogin.setError("Неверный код менеджера!");
    }

    @Override
    public void passwordError(){
        mEditTextPassword.requestFocus();
        mEditTextPassword.setError("Длина пароля должна быть не менее 6 символов");
    }

    @Override
    public void showLoading() {
//        ((MainActivity)context).showLoading();

        mButtonSignIn.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
//        ((MainActivity)context).hideLoading();
        mButtonSignIn.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

//    @Override
//    public void openLoginFragment() {
//
//    }

    @Override
    public void showError() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Ошибка авторизации", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void showMessage(String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void replaceFragment(Fragment fragment) {
        ((MainActivity)context).changeFragment(fragment, true);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unsubscribe();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
