package com.madlab.mmerch.ui.clients;

import com.madlab.mmerch.common.BaseView;
import com.madlab.mmerch.data.model.Client;

import java.util.List;

import moxy.viewstate.strategy.SkipStrategy;
import moxy.viewstate.strategy.StateStrategyType;

/**
 * Created by Toxo on 17.11.2019
 */

@StateStrategyType(SkipStrategy.class)
interface TodayClientsView extends BaseView {
    void addData(List<Client> clients);
}
