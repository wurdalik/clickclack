package com.madlab.mmerch.ui.monitoring;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.R;
import com.madlab.mmerch.data.model.Product;
import com.madlab.mmerch.ui.monitoring.adapter.ProductAdapter;

import java.util.ArrayList;

public class MonitoringFragment extends Fragment implements MonitoringContract.View{
    private Context context;
    private RecyclerView recyclerView;
    private ProductAdapter adapter;
    private MonitoringPresenter presenter;

    public static MonitoringFragment newInstance() {
        MonitoringFragment fragment = new MonitoringFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.monitoring_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        adapter = new ProductAdapter();
        recyclerView = view.findViewById(R.id.products);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new MonitoringPresenter(this);
        presenter.subscribe();
    }

    @Override
    public void addData(ArrayList<Product> products) {
        adapter.setData(products);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void replaceFragment(Fragment fragment) {

    }
}
