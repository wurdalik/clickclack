package com.madlab.mmerch.ui.visit;

import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Location;

import com.google.android.gms.location.LocationRequest;
import com.google.gson.JsonObject;
import com.madlab.mmerch.api.ApiUtils;
import com.madlab.mmerch.data.db.mappers.ClientEntityMapper;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BasePresenter;
import com.madlab.mmerch.ui.projects.ProjectsFragment;
import com.patloew.rxlocation.RxLocation;

import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import moxy.InjectViewState;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class VisitPresenter extends BasePresenter<VisitView> {
    private static final String CLIENT_KEY = "client";
    private static final String ADDRESS_KEY = "address";
    private VisitView view;

    private String OIIS = UUID.randomUUID().toString();

    private Client client;
    private Visit visit;

    private Location visitLocation;
    private String error;
    private double distance;

    private static boolean coinciedence = false;

    public static boolean location = false;
    private String clientID;


    private CompositeDisposable compositeDisposable;
    private SQLiteDatabase dataBase;
    private RxLocation rxLocation;
    private LocationRequest locationRequest;
    private PreferencesHelper preferences;

    VisitPresenter(SQLiteDatabase dataBase, RxLocation rxLocation, PreferencesHelper preferences, String clientID) {
        this.dataBase = dataBase;
        this.rxLocation = rxLocation;
        this.preferences = preferences;
        this.clientID = clientID;
        compositeDisposable = new CompositeDisposable();

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        init(clientID);
        getProjectsByClientID(clientID);
    }

    private LocationRequest getLocationRequest() {
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000);
        return locationRequest;
    }



    public Disposable init(String clientID) {
        getViewState().showLoading();
        visit = new Visit();

//        visit.setStartTime(Calendar.getInstance().getTime().getTime());
        visit.setStartTime(0);

        return Single.just(ClientEntityMapper.getClientByID(dataBase, clientID))
                .observeOn(AndroidSchedulers.mainThread())
                .flatMapCompletable(client -> {
                    visit.setClient(client);
                    return bindView(client);
                })
                .doFinally(() -> {
//                    getViewState().hideLoading();
//                        preferences.edit().putString("visit", visit.getId()).apply();
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().showMessage(throwable.getMessage());
                    }
                })
                .subscribe();

    }

    private Completable bindView(Client client) {
        return Completable.fromAction(() -> {
            getViewState().setClientTitle(client.getTitle());
            getViewState().setClientAddress(client.getAddress());
            getViewState().setChildfragment(ProjectsFragment.newInstance(client.getId()));
            visit.setProjects(client.getProjectsArray());
            if (visit.getCoordinates_latitude() == 0) {
                compositeDisposable.add(getAddress());
            } else {
                compositeDisposable.add(getAddressFromLocation(visit.getCoordinates_latitude(), visit.getCoordinates_longitude()));
            }
        })
                .subscribeOn(AndroidSchedulers.mainThread())
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().showMessage(throwable.getMessage());
                    }
                });
    }


    public Disposable getAddress() {
        getViewState().hideLocationRepeat();
        getViewState().showLoading();
        return checkSettings()
                .flatMapObservable((Function<Boolean, Observable<Location>>) aBoolean -> {
                    if (aBoolean) {
                        return getLocation();
                    } else {
                        throw new Exception("Необходимо включить GPS!");
                    }
                })
                .flatMap((Function<Location, Observable<Address>>) location -> {
                    if (location.getAccuracy() > 300) {
                        throw new Exception("Вы находитесь слишком далеко от точки!");
                    } else {
                        visit.setCoordinates_latitude(location.getLatitude());
                        visit.setCoordinates_longitude(location.getLongitude());
                        visit.setError(location.getAccuracy());
                        return rxLocation.geocoding().fromLocation(location).toObservable();
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getViewState().showLocationRepeat();
                        getViewState().showMessage(throwable.getMessage());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(address -> {
                    getViewState().hideLoading();
                    getViewState().showMessage("Местоположение определено!");
//                    fixme
//                        getViewState().setLocation(address.getAddressLine(0));

                    //show here
                    distance = calculateDistance(client.getCoordinates_latitude(), client.getCoordinates_longitude(), visit.getCoordinates_latitude(), visit.getCoordinates_longitude(), "K");
//                        if(distance>0.25){
//                            getViewState().showDistance(distance);
//                        }

                    compositeDisposable.dispose();
                });
    }

    private void getProjectsByClientID(String clientID){
        compositeDisposable.add(
                Observable.just(ClientEntityMapper.getClientByID(dataBase, clientID))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnError(throwable -> getViewState().showMessage(throwable.getMessage()))
                        .subscribe(client -> getViewState().addProjects(client.getProjects()))
        );
    }


    public void sendVisit() {
        getViewState().showLoading();
        String token = "Token " + preferences.getToken();
        String code = preferences.getUserCode();
        visit.setManagerID(code);



        Calendar calendar = Calendar.getInstance();
        String s = "123";
        //calendar.get(Calendar.MONTH)+1,
//        String data = String.format(Locale.getDefault(), "%d.%d.%d %d:%d:%d", calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.YEAR), calendar.get(calendar.HOUR_OF_DAY), calendar.get(calendar.MINUTE), calendar.get(calendar.SECOND));

//        visit.setEndTime(Calendar.getInstance().getTime().getTime());

        visit.setEndTime(0);



        ApiUtils.getSiteApi().sendVisit(token, visit).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                getViewState().showMessage("Визит отправлен!");
                getViewState().sendPhoto();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                getViewState().showMessage("Ошибка при отправке визита!");
                getViewState().close();
            }
        });
    }

    public Visit getVisit() {
        return visit;
    }


    private Disposable getAddressFromLocation(double latitude, double longitude) {
        Location location = new Location("");
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return rxLocation.geocoding()
                .fromLocation(location)
                .toObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(address -> getViewState().setClientAddress(address.getAddressLine(0)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        location = false;
    }

    public Single<Boolean> checkSettings() {
        return rxLocation.settings().checkAndHandleResolution(getLocationRequest())
                .doOnError(throwable -> getViewState().showMessage(throwable.getMessage()));
    }

    private Observable<Location> getLocation() {
        return rxLocation.location().updates(locationRequest)
                .filter(location -> location.getAccuracy() < 250)
                .retryWhen(trowable -> trowable
                        .take(5)
                        .delay(10, TimeUnit.SECONDS));
    }

    private double calculateDistance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "M") {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

}
