package com.madlab.mmerch.ui.clients;

import android.database.sqlite.SQLiteDatabase;

import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.prefs.PreferencesHelper;

import io.reactivex.disposables.CompositeDisposable;
import moxy.InjectViewState;
import moxy.MvpPresenter;

@InjectViewState
public class ListOfClientsPresenter extends MvpPresenter<ListOfClientsView> {
    private CompositeDisposable compositeDisposable;
    private PreferencesHelper preferencesHelper;
    private SQLiteDatabase dataBase;
    private Visit visit;

    public ListOfClientsPresenter(PreferencesHelper preferencesHelper, SQLiteDatabase dataBase){
        this.preferencesHelper = preferencesHelper;
        this.dataBase = dataBase;
        compositeDisposable = new CompositeDisposable();
    }


    public void unsubscribe() {
        compositeDisposable.clear();
    }


}
