package com.madlab.mmerch.ui.menu;

public class MenuPresenter implements MenuContract.Presenter {
    private MenuContract.View view;

    public MenuPresenter(MenuContract.View view){
        this.view = view;
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void setFragment() {

    }
}
