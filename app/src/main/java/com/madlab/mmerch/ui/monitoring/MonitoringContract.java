package com.madlab.mmerch.ui.monitoring;

import com.madlab.mmerch.common.BasePresenter;
import com.madlab.mmerch.common.BaseView;
import com.madlab.mmerch.data.model.Product;

import java.util.ArrayList;

public interface MonitoringContract{

    interface View extends BaseView {
        void addData(ArrayList<Product> products);
    }

    interface Presenter extends BasePresenter {
        void getData();

    }
}
