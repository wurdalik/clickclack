package com.madlab.mmerch.ui.cabinet;

import com.madlab.mmerch.prefs.PreferencesHelper;

public class CabinetPresenter implements CabinetContract.Presenter {
    private CabinetContract.View view;
    private PreferencesHelper preferencesHelper;

    public CabinetPresenter(CabinetContract.View view, PreferencesHelper preferencesHelper){
        this.view = view;
        this.preferencesHelper = preferencesHelper;
    }

    public void logout(){
        view.logOut();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void setFragment() {

    }
}
