package com.madlab.mmerch.ui.visitlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.madlab.mmerch.R;
import com.madlab.mmerch.data.db.mappers.ClientEntityMapper;
import com.madlab.mmerch.data.model.Visit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class VisitAdapter extends RecyclerView.Adapter<VisitAdapter.VisitHolder> {
    private final ArrayList<Visit> visits = new ArrayList<>();

    @NonNull
    @Override
    public VisitHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VisitHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.visit_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VisitHolder holder, int position) {
        holder.bind(visits.get(position));
    }

    @Override
    public int getItemCount() {
        return visits.size();
    }

    public void setData(ArrayList<Visit> visits){
        this.visits.clear();
        this.visits.addAll(visits);
        notifyDataSetChanged();
    }

    public static class VisitHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView subtitle;
        TextView clock;
        ImageView photo;

        public VisitHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.visit_title);
            subtitle = itemView.findViewById(R.id.visit_subtitle);
            clock = itemView.findViewById(R.id.time);
            photo = itemView.findViewById(R.id.photo);
        }

        public void bind(Visit visit){
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
            title.setText(visit.getClient().getTitle());
            subtitle.setText(visit.getClient().getAddress());
            clock.setText(String.format("%s - %s", dateFormat.format(new Date(visit.getStartTime())), dateFormat.format(new Date(visit.getEndTime()))));
            if(visit.getPhotoPath()!=null){
                Glide.with(itemView)
                        .load(visit.getPhotoPath())
                        .into(photo);

            }
        }
    }
}
