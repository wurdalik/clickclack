package com.madlab.mmerch.ui.clients;

import com.madlab.mmerch.common.BaseView;
import com.madlab.mmerch.data.model.Client;

import java.util.List;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.AddToEndStrategy;
import moxy.viewstate.strategy.SkipStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(SkipStrategy.class)
public interface ListOfClientsView extends BaseView {

        void addData(List<Client> clients);
}
