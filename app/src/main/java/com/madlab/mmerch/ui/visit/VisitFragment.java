package com.madlab.mmerch.ui.visit;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.madlab.mmerch.ItemClickSupport;
import com.madlab.mmerch.R;
import com.madlab.mmerch.RequireTestDependency;
import com.madlab.mmerch.api.ApiUtils;
import com.madlab.mmerch.data.db.mappers.ClientEntityMapper;
import com.madlab.mmerch.data.db.mappers.VisitEntityMapper;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Project;
import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.MainActivity;
import com.madlab.mmerch.ui.base.BaseFragment;
import com.madlab.mmerch.ui.projects.ProjectsAdapter;
import com.patloew.rxlocation.RxLocation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitFragment extends BaseFragment implements View.OnClickListener, VisitView, RequireTestDependency, ItemClickSupport.OnItemClickListener{
    private static final String TAG = VisitFragment.class.getCanonicalName();
    private static final int REQUEST_TAKE_PHOTO = 1418;
    private Context context;

    private String visitCode;
    private SQLiteDatabase dataBase;
    private String clientId;


    private RelativeLayout tasks;
    private AppCompatTextView tv_title;
    private AppCompatTextView tv_address;
    private LinearLayout repeatLocation;
    private RecyclerView projectsRecycler;
    private AppCompatTextView tv_location;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private Button locationRepeat;

    private ProjectsAdapter projectsAdapter;

    private View photo_start;

    private String data;

    private FloatingActionButton send;
    private CheckBox cb_photo_start;

    private RxLocation rxLocation;

    private PreferencesHelper preference;

    private String currentPhotoPath;

    @InjectPresenter
    VisitPresenter presenter;

    @ProvidePresenter
    VisitPresenter providePresenter(){
        return new VisitPresenter(dataBase, rxLocation, preference, getArguments().getString("client"));
    }

    public static VisitFragment newInstance(String clientCode){
        Bundle args = new Bundle();
        args.putString("client", clientCode);
        VisitFragment visitFragment = new VisitFragment();
        visitFragment.setArguments(args);
        return visitFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        visitCode = getArguments().getString("client");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_visit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        repeatLocation = view.findViewById(R.id.repeatLocation);
        locationRepeat = view.findViewById(R.id.repeat);
        tasks = view.findViewById(R.id.tasks_list);
        toolbar = view.findViewById(R.id.visit_fragment_toolbar);
        projectsRecycler = view.findViewById(R.id.projects_recycler);
        projectsRecycler.setLayoutManager(new LinearLayoutManager(context));
        projectsRecycler.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
//        tasksRecycler = view.findViewById(R.id.tasks_recycler);
        toolbar.setTitle("Визит");
        progressBar = view.findViewById(R.id.visit_pb);
        tv_title = view.findViewById(R.id.clientTitle);
        tv_address = view.findViewById(R.id.clientAddress);
        photo_start = view.findViewById(R.id.photo_start);
        cb_photo_start = view.findViewById(R.id.there_are_photo);
        send = view.findViewById(R.id.send_visit);
        locationRepeat.setOnClickListener(this);
        projectsAdapter = new ProjectsAdapter();
        projectsRecycler.setAdapter(projectsAdapter);
        send.setOnClickListener(this);
        ItemClickSupport.addTo(projectsRecycler).setOnItemClickListener(this);

    }

    private void setTasks(){
        ItemClickSupport.removeFrom(projectsRecycler);
        projectsRecycler.setVisibility(View.INVISIBLE);
        tasks.setVisibility(View.VISIBLE);
        photo_start.setOnClickListener(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    protected VisitPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        ((MainActivity)context).changeFragment(fragment, true);
    }

    @Override
    public void showLocationRepeat() {
        hideLoading();
        locationRepeat.setEnabled(true);
        repeatLocation.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLocationRepeat(){
        locationRepeat.setEnabled(false);
        repeatLocation.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setChildfragment(Fragment fragment) {
//        getChildFragmentManager().beginTransaction()
//                .replace(R.id.visit_fragment_container, fragment)
//                .addToBackStack(null)
//                .commit();
    }

    @Override
    public void close(){
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void sendPhoto() {
        VisitEntityMapper.insert(dataBase, presenter.getVisit());
        if(currentPhotoPath!=null){
            sendPhoto(currentPhotoPath);
        }
        else {
            close();
        }
//        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void addProjects(ArrayList<Project> projects) {
        projectsAdapter.addData(projects);
    }

    @Override
    public void setClientTitle(String title) {
        tv_title.setText(title);
    }

    @Override
    public void setClientAddress(String address) {
        tv_address.setText(address);
    }

    @Override
    public void setLocation(String location) {
        tv_location.setText(location);
    }

    @Override
    public void showDistance(Double distance) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(String.format("Вы находитесь на расстоянии %1$,.1f км от адреса клиента", distance));
        builder.setPositiveButton("Ок", (dialog, which) -> {

        });
        builder.setNegativeButton("Отмена", (dialog, which) -> {

        });
        builder.create();
        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.photo_start:
                if(progressBar.getVisibility() == View.VISIBLE){
                    Toast.makeText(context, "Дождитесь определения местоположения", Toast.LENGTH_SHORT).show();
                }
                else {
                    dispatchTakePictureIntent();
                }
                break;
            case R.id.send_visit:
                if(progressBar.getVisibility() == View.VISIBLE){
                    Toast.makeText(context, "Дождитесь определения местоположения", Toast.LENGTH_SHORT).show();
                }
                else {
                    presenter.sendVisit();
                }
                    break;
            case R.id.repeat:
                presenter.getAddress();
                break;
        }
    }

    //fixme changing



    private File createImageFile(String visit) throws IOException {
        // Create an image file name
        String photoName = null;
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                visit,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        currentPhotoPath = image.getAbsolutePath();

        return image;
    }

//    {MAIN_PHOTO}_{VISIT_ID}_{CLIENT_ID}_{MANAGER_ID}_{DATE}_{OTHER_ID}_{PROJECT_ID}

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            File photoFile = null;
            try{
                photoFile = createImageFile("photo");


            } catch (IOException e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                cb_photo_start.setChecked(true);
            }
            else{
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                cb_photo_start.setChecked(true);
            }

        }

    }

    public void sendPhoto(String path){
        String token = "Token "+ preference.getToken();

        File f = new File(path);
        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), f);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("image", f.getName(), fileReqBody);

        //fixme only for TEST!
        Visit test = presenter.getVisit();

        ApiUtils.getSiteApi().sendPhotov2(token, "0", presenter.getVisit().getClient().getId(), String.valueOf(test.getStartTime()), "1", presenter.getVisit().getClient().getProjectsArray(), presenter.getVisit().getId(), presenter.getVisit().getManagerID(), part).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                switch (response.code()){
                    case 200: Toast.makeText(context, "Фото успешно отправлено!", Toast.LENGTH_SHORT).show();
                        getActivity().getSupportFragmentManager().popBackStack();
                        break;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Toast.makeText(context, "не удалось отправить фото", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

    }

    public  String getMimeType(Uri uri){
        String mimeType = null;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = getActivity().getApplicationContext().getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return mimeType;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_IMAGE_CAPTURE:

                break;

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location) {
        this.dataBase = dataBase;
        this.preference = preferences;
        this.rxLocation = location;
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
        switch (recyclerView.getId()){
            case R.id.projects_recycler: setTasks();
        }
    }
}
