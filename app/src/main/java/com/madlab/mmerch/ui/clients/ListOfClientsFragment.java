package com.madlab.mmerch.ui.clients;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.madlab.mmerch.R;
import com.madlab.mmerch.ui.clients.page_adapter.ClientsFragmentAdapter;

import moxy.MvpAppCompatFragment;

public class ListOfClientsFragment extends MvpAppCompatFragment implements Toolbar.OnMenuItemClickListener{
    private Context context;
    private Toolbar toolbar;

    private TabLayout tabLayout;
    private ViewPager2 clientsViewPager;
    private ClientsFragmentAdapter adapter;

    private ProgressBar progressBar;

    public static ListOfClientsFragment newInstance() {
        return new ListOfClientsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_of_clients, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        toolbar = view.findViewById(R.id.list_clients_fragment_toolbar);
//        toolbar.inflateMenu(R.menu.clients_fragment_menu);
//        toolbar.setOnMenuItemClickListener(this);
        tabLayout = view.findViewById(R.id.clients_tab);
        clientsViewPager = view.findViewById(R.id.clients_view_pager);
        adapter = new ClientsFragmentAdapter(getActivity());
        adapter.addFragment(new TodayClientsFragment(), "Сегодня");
        adapter.addFragment(new AllClientsFragment(), "Все");
        clientsViewPager.setAdapter(adapter);
        new TabLayoutMediator(tabLayout, clientsViewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(adapter.getTabTitle(position));
            }
        }).attach();
    }

    public void Update(){
        adapter.updateData();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.update_data:

//                ApiUtils.getSiteApi().getTime("http://worldtimeapi.org/api/ip/").enqueue(new Callback<JsonObject>() {
//                    @Override
//                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                        String s = "123";
//                    }
//
//                    @Override
//                    public void onFailure(Call<JsonObject> call, Throwable t) {
//                        String s = "123";
//                    }
//                });


                adapter.updateData();
                break;
        }
        return false;
    }
}
