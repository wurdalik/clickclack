package com.madlab.mmerch.ui.clients;

import com.madlab.mmerch.common.BaseView;
import com.madlab.mmerch.data.model.Client;

import java.util.ArrayList;

import moxy.viewstate.strategy.AddToEndSingleStrategy;
import moxy.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface AllClientsView extends BaseView {
    void addData(ArrayList<Client>clients);
}
