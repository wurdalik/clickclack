package com.madlab.mmerch.ui.base;

import moxy.MvpAppCompatFragment;

public abstract class BaseFragment extends MvpAppCompatFragment {

    protected abstract BasePresenter getPresenter();



    @Override
    public void onDetach() {
        if(getPresenter()!=null){
            getPresenter().disposeAll();
        }
        super.onDetach();
    }
}
