package com.madlab.mmerch.ui.projects;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.madlab.mmerch.ItemClickSupport;
import com.madlab.mmerch.R;
import com.madlab.mmerch.RequireTestDependency;
import com.madlab.mmerch.data.model.Project;
import com.madlab.mmerch.prefs.PreferencesHelper;
import com.madlab.mmerch.ui.base.BaseFragment;
import com.madlab.mmerch.ui.base.BasePresenter;
import com.patloew.rxlocation.RxLocation;

import java.util.ArrayList;

import moxy.presenter.InjectPresenter;
import moxy.presenter.ProvidePresenter;

public class ProjectsFragment extends BaseFragment implements ProjectsView, RequireTestDependency, ItemClickSupport.OnItemClickListener {
    private RecyclerView recyclerView;
    private ProjectsAdapter projectsAdapter;

    private SQLiteDatabase database;
    private PreferencesHelper preferences;

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    @InjectPresenter
    ProjectsPresenter presenter;

    @ProvidePresenter
    ProjectsPresenter providePresenter(){
        return new ProjectsPresenter(database, preferences, getArguments().getString("clientID"));
    }

    public static ProjectsFragment newInstance(String clientID){
        Bundle args = new Bundle();
        args.putString("clientID", clientID);
        ProjectsFragment fragment = new ProjectsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setDataBase(SQLiteDatabase dataBase, PreferencesHelper preferences, RxLocation location) {
        this.database = dataBase;
        this.preferences = preferences;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.projects_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView = view.findViewById(R.id.projects_recycler);
        projectsAdapter = new ProjectsAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(projectsAdapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(this);
//        presenter.getProjectsByClientID(getArguments().getString("clientID"));
    }

    @Override
    public void addData(ArrayList<Project> projects) {
        projectsAdapter.addData(projects);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void replaceFragment(Fragment fragment) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//        getParentFragment().getChildFragmentManager().beginTransaction()
//                .replace(R.id.visit_fragment_container, TasksFragment.newInstance()).commit();
    }
}
