package com.madlab.mmerch.ui.menu;

import com.madlab.mmerch.common.BasePresenter;
import com.madlab.mmerch.common.BaseView;

public interface MenuContract {
    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter{

    }
}
