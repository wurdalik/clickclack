package com.madlab.mmerch;

public interface PermissionHandler {
    boolean checkHasPermission();

    void requestPermission();
}
