package com.madlab.mmerch.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.madlab.mmerch.BuildConfig;
import com.madlab.mmerch.data.model.Client;
import com.madlab.mmerch.data.model.Visit;
import com.madlab.mmerch.data.net.ClientDeserializer;
import com.madlab.mmerch.data.net.VisitSerializer;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiUtils {
    private static OkHttpClient client;
    private static Retrofit retrofit;
    private static Gson gson;
    private static Api siteApi;
    private static AlternativeApi alternativeApi;


    public static Retrofit getRetrofit(String url){
        if(gson==null){
            gson = new GsonBuilder()
                    .registerTypeAdapter(Client.class, new ClientDeserializer())
                    .registerTypeAdapter(Visit.class, new VisitSerializer())
                    .create();
        }

        if(retrofit==null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(getClient())
                    .build();
        }
        return retrofit;
    }

    public static Api getSiteApi(){
        if(siteApi == null){
            siteApi = getRetrofit(BuildConfig.BASE_URL).create(Api.class);
        }
        return siteApi;
    }

    public static OkHttpClient getClient() {
        if(client==null){
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
            builder.connectTimeout(60, TimeUnit.SECONDS);
            builder.readTimeout(60, TimeUnit.SECONDS);
            builder.writeTimeout(60, TimeUnit.SECONDS);
            client = builder.build();
        }
        return client;
    }

    public static AlternativeApi getAlternativeApi(){
        if(alternativeApi == null){
            alternativeApi = getRetrofit(BuildConfig.ALT_URL).create(AlternativeApi.class);
        }
        return alternativeApi;
    }

}
