package com.madlab.mmerch.api;

import com.google.gson.JsonObject;
import com.madlab.mmerch.data.model.Clients;
import com.madlab.mmerch.data.model.Visit;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface Api {

    @POST("manager/login/")
    Observable<JsonObject> doServerLoginApiCall(@Body JsonObject jsonObject);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("clients?")
    Observable<Clients> getClients(@Header("Authorization") String token, @Query("managerCode") String userCode);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("visits/create")
    Call<JsonObject> sendVisit(@Header("Authorization") String token, @Body Visit visit);


    @Headers({
            "Content-Type: application/json"
    })
    @Multipart
    @POST("visits/upload-image")
    Call<JsonObject>  sendPhoto(@Header("Authorization") String token, @Part("0_cd585ce2-1b34-4a67-87f8-1ae49b13c296_С11316_20191031_095525_001") MultipartBody.Part file);

    @Multipart
    @PUT("visits/upload-image")
    Call<JsonObject> sendPhotov2(@Header("Authorization") String token,
                                 @Query("main_photo") String main_photo,
                                 @Query("client_id") String client_id,
                                 @Query("date") String date,
                                 @Query("other_id") String other_id,
                                 @Query("project_id") String [] project_id,
                                 @Query("visit_id") String visit_id,
                                 @Query("manager_id") String manager_id,
                                 @Part MultipartBody.Part image);

    @GET
    Call<JsonObject> getTime(@Url String utl);
}
