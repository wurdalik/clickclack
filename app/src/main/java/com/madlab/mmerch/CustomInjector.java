package com.madlab.mmerch;

import android.app.Activity;
import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.madlab.mmerch.prefs.PreferencesHelper;
import com.patloew.rxlocation.RxLocation;

public class CustomInjector extends FragmentManager.FragmentLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
    private final SQLiteDatabase database;
    private final PreferencesHelper preferences;
    private final RxLocation location;

    public CustomInjector(SQLiteDatabase database, PreferencesHelper preferences, RxLocation location){
        this.database = database;
        this.preferences = preferences;
        this.location = location;
    }

    @Override
    public void onFragmentPreCreated(@NonNull FragmentManager fm, @NonNull Fragment f, @Nullable Bundle savedInstanceState) {
        if(f instanceof RequireTestDependency){
            ((RequireTestDependency)f).setDataBase(database, preferences, location);
        }
        if(f instanceof RequirePreference){
            ((RequirePreference)f).setPreference(preferences);
        }
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if(activity instanceof AppCompatActivity){
            ((AppCompatActivity)activity).getSupportFragmentManager().registerFragmentLifecycleCallbacks(this, true);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
