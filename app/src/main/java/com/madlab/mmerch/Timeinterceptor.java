//package com.madlab.mmerch;
//
//import android.util.Log;
//import android.widget.Toast;
//
//import org.apache.commons.net.ntp.NTPUDPClient;
//import org.apache.commons.net.ntp.TimeInfo;
//
//import java.net.InetAddress;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.Locale;
//
//import io.reactivex.Observable;
//import io.reactivex.Scheduler;
//import io.reactivex.Single;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.CompositeDisposable;
//import io.reactivex.functions.Consumer;
//import io.reactivex.schedulers.Schedulers;
//
//public class Timeinterceptor {
//    String data = null;
//
//
//    public String getTimeForPhoto(){
//
//        getRealTime().subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<String>() {
//                    @Override
//                    public void accept(String s) throws Exception {
//                        data = s;
//                    }
//                });
//        return data;
//    }
//
//    static private Observable<String> getRealTime() {
//        Observable<String>observable = Observable.create(o->{
//            try {
//                NTPUDPClient timeClient = new NTPUDPClient();
//                timeClient.setDefaultTimeout(5000);
//                InetAddress inetAddress = InetAddress.getByName("0.ru.pool.ntp.org");
//                TimeInfo timeInfo = timeClient.getTime(inetAddress);
//                long returnTime = timeInfo.getMessage().getTransmitTimeStamp().getTime();
//                Date time = new Date(returnTime);
//                Calendar calendar = Calendar.getInstance();
//                calendar.setTime(time);
//
//                o.onNext(String.format(Locale.getDefault(), "%d%d%d", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
//
//            } catch (Exception e) {
//                o.onError(e);
//            }
//            o.onComplete();
//        });
//        return observable;
//    }
//}
